# Continuous Delivery tutorial using GoCD

This repository contains resource for setting up a GoCD server and agents for Java and dotnet core in Docker.

Please note that the images are quite large (especially the dotnet core agent which is 2.04 GB) so make sure you have plenty of space and don't use a metered internet connection or you'll quickly use up your data plan.
